package af.abcd.voice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class WriteAct extends AppCompatActivity {
    ImageButton btnWriteClose;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write);

        btnWriteClose = findViewById(R.id.btnWriteClose);
        btnWriteClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WriteAct.this,MainActivity.class));
                finish();
            }
        });

    }
}
